# Walt O'Connor Quack Parser
Instructions:
Build the docker image (with sudo if needed)
`make image`

Run the docker image (with sudo if needed)
`docker run -it proj2_waltoconnor`

Build the project (you should be put in the the project folder by default)
`make`

Run the project
`./bin/parser ./samples/Pt.qk`



NOTE:
Probably won't compile on Mac (out of docker container) due to linker error (https://piazza.com/class/jxfmgc4s4ga56y?cid=15)
Compiles and runs on Windows Subsystem for Linux Ubuntu 18.04
Compiles and runs on Ubuntu Server 19.04 

Through a very strange sequence of events, I find myself without my usual docker host and have no other hosts that have the complete set of virtualization capabilities that Docker needs to run (Email me if you want the gory details). Hopefully the docker file I provided works because its just a slightly modified version of the base docker file, but I haven't tested that. 
